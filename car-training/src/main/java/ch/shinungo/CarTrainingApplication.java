package ch.shinungo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarTrainingApplication.class, args);
	}
	//TEST 12.00
}