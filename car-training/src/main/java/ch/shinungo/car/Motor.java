package ch.shinungo.car;

public interface Motor {

	public void startMotor();
	public void stopMotor();
}