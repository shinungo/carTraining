package ch.shinungo.car;

public class MotorBlock {
	
	private Motor motor;
	private Cylinder cylinder; 
	
	public MotorBlock(Motor mo, Cylinder cy) {
		this.motor = mo;
		this.cylinder = cy;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Cylinder getCylinder() {
		return cylinder;
	}

	public void setCylinder(Cylinder cylinder) {
		this.cylinder = cylinder;
	}
}