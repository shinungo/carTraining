package ch.shinungo.car;

public class Auto {
	
	private MotorBlock mb; 
	private Hupe hupe;
	private Tyre tyre; 
	private boolean isEngineStarted = false;

	public Auto(Motor motor, Hupe hupe, Cylinder cylinder, Tyre tyre) {
		this.mb = new MotorBlock(motor, cylinder);
		this.hupe = hupe;
		this.tyre = tyre;
	}
	
	public Auto(MotorBlock mb, Hupe hupe, Tyre tyre) {
		this.mb = mb;
		this.hupe = hupe;
		this.tyre = tyre;
	}

	// this ruft vorherigen Konstruktor auf: Auto "this" enthält Motor & Neue StandardHupe 
	public Auto(Motor motor, Cylinder cylinder) {
		this(motor, new HupeStandard(), cylinder, new TyreWinter());
	}

	public Auto() {
		this(new MotorSport(), new HupeStandard(), new CylinderDouble(), new TyreSummer());
	}

	public Auto(Cylinder cylinder) {
		this(new MotorStandard(), cylinder);
	}
		
// Mit mb.getCylinder zündet Motorblock den Cylinder.   
	public void fireCylinder() {
		mb.getCylinder().zuenden();
	}
	
	public void showTyre() {
		tyre.choosaTyre();
	}
	
	public Auto makeMotorBlockCar() {
		Auto auto = new Auto(new MotorBlock(new MotorLimo(), new CylinderDouble()), new HupeStandard(), new TyreWinter());
		return auto;
	}	

	//Motor über Motorenblock gesteuert. 
	public void startEngine() {
		isEngineStarted = true;
		mb.getMotor().startMotor();
	}

	
	public void stopEngine() {
		isEngineStarted = false;
		System.out.print("Auto gestoppt |");
		mb.getMotor().stopMotor();
	}

	public String pressHupe() {
		if(isEngineStarted()) {
			return hupe.hupe();						
		}else {
			return null;
		}
	}
	
	public boolean isEngineStarted() {
		return isEngineStarted;
	}
}