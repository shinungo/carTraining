package ch.shinungo.car;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		Main m = new Main();
		List<Auto> autoList = new ArrayList<Auto>();
		autoList.add(m.makeMotorBlockCar());
		autoList.add(m.makeFastCar());
		autoList.add(m.makeSportCar());
		autoList.add(m.makeLimoCar());
	
	// Neues Auto mit LimoMotor - Per Default mit StandardHupe
		autoList.add(new Auto(new MotorLimo(), new CylinderDouble())); 
		autoList.add(new Auto());
		autoList.add(new Auto(new MotorStandard(), new CylinderSingle()));

	// Such jedes Auto (a) (= ein einfacher Index) dieser Liste, und führ Methode staStoHu aus
		autoList.forEach(a -> m.staStoHu(a)); 
	}

	public void staStoHu(Auto auto) {
		auto.startEngine();
		auto.pressHupe();
		auto.fireCylinder();
		auto.showTyre();
		auto.stopEngine();
		System.out.println("------------------------------------------------------------------------------------------------------------------");
	}
	
	public Auto makeFastCar() {
		Auto auto = new Auto(new MotorSport(), new HupeHorn(), new CylinderDouble(), new TyreSummer());
		return auto;
		
	}
	
	public Auto makeMotorBlockCar() {
		Auto auto = new Auto(new MotorBlock(new MotorLimo(), new CylinderDouble()), new HupeStandard(), new TyreWinter());
		return auto;
	}
	

	public Auto makeLimoCar() {
		Motor motor = new MotorLimo();
		Tyre ty = new TyreSummer();
		Cylinder cylinder = new CylinderSingle();
		Auto auto = new Auto(motor, new HupeHorn(),cylinder,ty);
		return auto;
	}
	
	public Auto makeSportCar() {
		Motor motor = new MotorSport();
		Tyre ty = new TyreWinter();
		Cylinder cylinder = new CylinderDouble();
		Auto auto = new Auto(motor, new HupeRing(),cylinder, ty);
		return auto;
	}
	public void test() {
		System.out.println("test");
	}
}