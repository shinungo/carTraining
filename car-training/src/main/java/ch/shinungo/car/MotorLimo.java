package ch.shinungo.car;

public class MotorLimo implements Motor {

	@Override
	public void startMotor() {
		System.out.print(" Motor Limo started |");
	}

	@Override
	public void stopMotor() {
		System.out.println(" Motor Limo stopped |");
	}
}