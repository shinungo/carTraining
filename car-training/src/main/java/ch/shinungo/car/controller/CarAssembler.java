package ch.shinungo.car.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.shinungo.car.Auto;
import ch.shinungo.car.Cylinder;
import ch.shinungo.car.CylinderDouble;
import ch.shinungo.car.HupeHorn;
import ch.shinungo.car.Hupe;
import ch.shinungo.car.MotorLimo;
import ch.shinungo.car.Motor;
import ch.shinungo.car.HupeRing;
import ch.shinungo.car.CylinderSingle;
import ch.shinungo.car.MotorSport;
import ch.shinungo.car.MotorStandard;
import ch.shinungo.car.Tyre;
import ch.shinungo.car.TyreSummer;
import ch.shinungo.car.TyreWinter;

@RestController
public class CarAssembler {
	/**
	 * Durch eingabe von: http://localhost:8080/?motor=limo&hupe=ring&cylinder=double&tyre=winter
	 * können beim starten dieses Programmes die einzelnen Teile aufgerufen und zurückgegeben werden. 
	 */
	
	@GetMapping({"/", "/home"})
	public String home (@RequestParam(value="motor", defaultValue="standard") String motor,
						@RequestParam(value="hupe", defaultValue="ring") String hupe,
						@RequestParam(value="cylinder", defaultValue="single") String cylinder,
						@RequestParam(value="tyre", defaultValue="summer") String tyre){

	String msg = "";
	String msgHupe = "";
	String msgCylinder = "";
	String msgTyre = ""; 
	String msgWelcome = "Aufruf durch: /?motor &hupe &cylinder &tyre "; 
	
	Motor mm = null;
	Hupe hu = null;
	Cylinder cy = null; 
	Tyre ty = null; 
		
	switch(motor) {
	case "standard":
		mm = new MotorStandard();
		msg = "standard Motor gewählt |";
		break;
	case "limo":
		mm = new MotorLimo(); 
		msg = "limo gewählt |";
		break;
	case "sport":
		mm = new MotorSport();
		msg = "sport gewählt |";
		break;
	default:
		msg = "Bitte 'limo', 'sport' oder 'standard' eingeben";
	}

	if (mm == null) {
		return msg;
	}

	switch(hupe) {
	case "horn":
		hu = new HupeHorn();
		msgHupe = "HornHupe gewählt |";
		break;
	case "ring":
		hu = new HupeRing(); 
		msgHupe = "RingHupe gewählt |";
		break;
	default:
		msgHupe = "Bitte 'horn' oder 'ring' zur Wahl der Hupe  eingeben";
	}

	if (hu == null) {
		return msgHupe;
	}

	
	switch(cylinder) {
	case "single":
		cy = new CylinderSingle();
		msgCylinder = "Cylinder single gewählt |";
		break;
	case "double":
		cy = new CylinderDouble(); 
		msgCylinder = "Cylinder double gewählt |";
		break;
	default:
		msgCylinder = "Bitte 'single' oder 'double' zur Cylinder Wahl  eingeben";
	}

	if (cy == null) {
		return msgCylinder;
	}
	


	switch(tyre) {
	case "winter":
		ty = new TyreWinter();
		msgTyre = " Tyre winter gewählt |";
		break;
	case "summer":
		ty = new TyreSummer(); 
		msgTyre = "summer gewählt |";
		break;
	default:
		msgTyre = "Bitte 'summer' oder 'winter' zur Tyre Wahl  eingeben";
	}

	if (ty == null) {
		return msgTyre;
	}

	Auto aa = new Auto(mm, hu, cy, ty);

	//OUTPUT comandline 
	aa.startEngine();
	aa.pressHupe();
	aa.fireCylinder();
	aa.showTyre();
	System.out.println("XXXXXXXXXXXXXXXX");
	
	//OUTPUT Localhost:
	String endMessage = msgWelcome + "</br>" + msg + " " + msgHupe + " " + msgCylinder + " " + msgTyre ; 
	return endMessage; 
	}
}