package ch.shinungo.car.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ch.shinungo.car.Auto;

public class TestAuto {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testHupe()  throws Exception {
		Auto a = new Auto();
		a.startEngine();
		String pressHupe = a.pressHupe();
		assertEquals("Hupe Standard", pressHupe);
	}
}